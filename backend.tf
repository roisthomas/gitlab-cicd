terraform {
  backend "s3" {
    bucket = "mybucketrois2003"
    key    = "state"
    region = "us-east-1"
    dynamodb_table = "terraform-lock" 
    encrypt        = true 
  }
}
