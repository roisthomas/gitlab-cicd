module "vpc" {
    source = "./vpc"
  
}

module "ec2" {
    source = "./ec2"
    sn = module.vpc.pub_sub1
    sg = module.vpc.sg  
}